{
  include "mixins/created_at.m5o"
  include "mixins/updated_at.m5o"
  include "mixins/closed_at.m5o"

  version = 1
  sql_table_name = gitlab_merge_requests
  name = gitlab_merge_requests
  columns {
    merge_request_id {
      primary_key = true
      hidden = true
      type = string
      sql = "{{table}}.merge_request_id"
    }
    project_id {
      label = Project ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.project_id"
    }
    milestone_id {
      label = Milestone ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.milestone_id"
    }
    author_id {
      label = Author ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.author_id"
    }
    assignee_id {
      label = Assignee ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.assignee_id"
    }
    closed_by_id {
      label = Closed by ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.closed_by_id"
    }
    merged_by_id {
      label = Merged by ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.merged_by_id"
    }
    title {
      label = Title
      description = MR Title
      type = string
      sql = "{{table}}.title"
    }
    state {
      label = State
      description = MR State
      type = string
      sql = "{{table}}.state"
    }
    labels_str {
      label = Labels (for filtering)
      description = Labels (for filtering)
      type = string
      sql = "{{table}}.labels_str"
    }
    created_year {
      label = Created Year
      description = Year the MR was Created
      type = string
      sql = "{{table}}.created_year"
    }
    created_quarter {
      label = Created Quarter
      description = Quarter the MR was Created
      type = string
      sql = "{{table}}.created_quarter"
    }
    created_month {
      label = Created Month
      description = Month the MR was Created
      type = string
      sql = "{{table}}.created_month"
    }
    created_week {
      label = Created Week
      description = Week the MR was Created
      type = string
      sql = "{{table}}.created_week"
    }
    created_day {
      label = Created Day
      description = Day the MR was Created
      type = string
      sql = "{{table}}.created_day"
    }
    merged_year {
      label = Merged Year
      description = Year the MR was Merged
      type = string
      sql = "{{table}}.merged_year"
    }
    merged_quarter {
      label = Merged Quarter
      description = Quarter the MR was Merged
      type = string
      sql = "{{table}}.merged_quarter"
    }
    merged_month {
      label = Merged Month
      description = Month the MR was Merged
      type = string
      sql = "{{table}}.merged_month"
    }
    merged_week {
      label = Merged Week
      description = Week the MR was Merged
      type = string
      sql = "{{table}}.merged_week"
    }
    merged_day {
      label = Merged Day
      description = Day the MR was Merged
      type = string
      sql = "{{table}}.merged_day"
    }
    closed_year {
      label = Closed Year
      description = Year the MR was Closed
      type = string
      sql = "{{table}}.closed_year"
    }
    closed_quarter {
      label = Closed Quarter
      description = Quarter the MR was Closed
      type = string
      sql = "{{table}}.closed_quarter"
    }
    closed_month {
      label = Closed Month
      description = Month the MR was Closed
      type = string
      sql = "{{table}}.closed_month"
    }
    closed_week {
      label = Closed Week
      description = Week the MR was Closed
      type = string
      sql = "{{table}}.closed_week"
    }
    closed_day {
      label = Closed Day
      description = Day the MR was Closed
      type = string
      sql = "{{table}}.closed_day"
    }
    merged_at {
      label = Merged Date
      type = time
      sql = "{{table}}.merged_at"
    }
  }
  timeframes {
    merged_at {
      include "mixins/_timeframe.proto.m5o"
      label = Merged At
      sql = "{{table}}.merged_at"
    }
  }
  aggregates {
    total_merge_requests {
      label = Total Merge Requests
      description = Total Merge Requests
      type = count
      sql = "{{table}}.merge_request_id"
    }
    total_open_mrs {
      label = Total Open MRs
      description = Total Open MRs
      type = sum
      sql = "{{table}}.state_open_mrs"
    }
    total_merged_mrs {
      label = Total Merged MRs
      description = Total Merged MRs
      type = sum
      sql = "{{table}}.state_merged_mrs"
    }
    total_closed_mrs {
      label = Total Closed MRs
      description = Total Closed MRs
      type = sum
      sql = "{{table}}.state_closed_mrs"
    }
    avg_hours_to_close_mr {
      label = Average Hours to Close
      description = Average Hours to Close
      type = avg
      sql = "{{table}}.hours_to_close"
    }
    avg_days_to_close_mr {
      label = Average Days to Close
      description = Average Days to Close
      type = avg
      sql = "{{table}}.days_to_close"
    }
    avg_hours_to_merge_mr {
      label = Average Hours to Merge
      description = Average Hours to Merge
      type = avg
      sql = "{{table}}.hours_to_merge"
    }
    avg_days_to_merge_mr {
      label = Average Days to Merge
      description = Average Days to Merge
      type = avg
      sql = "{{table}}.days_to_merge"
    }
    total_mr_upvotes {
      label = Total MR Upvotes
      description = Total MR Upvotes
      type = sum
      sql = "{{table}}.upvotes"
    }
    total_mr_downvotes {
      label = Total MR Downvotes
      description = Total MR Downvotes
      type = sum
      sql = "{{table}}.downvotes"
    }
    total_mr_user_notes {
      label = Total MR Comments
      description = Total MR Comments
      type = sum
      sql = "{{table}}.user_notes_count"
    }
  }
}
