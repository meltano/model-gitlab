from setuptools import setup, find_packages

setup(
    name='model-gitlab',
    version='0.4',
    description='Meltano .m5o models for data fetched using the GitLab API',
    packages=find_packages(),
    package_data={'models': ['**/*.m5o', '*.m5o']},
    install_requires=[],
)
